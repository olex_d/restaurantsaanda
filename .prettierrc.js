module.exports = {
  printWidth: 100,
  endOfLine: 'lf',
  tabWidth: 2,
  useTabs: false,
  semi: true,
  singleQuote: true,
  trailingComma: 'none',
  arrowParens: 'avoid'
};
