import React, { Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Loader from '../../components/Loader';

const Landing = React.lazy(() => import('../../screens/Landing/containers/LandingView'));
const RestaurantView = React.lazy(
  () => import('../../screens/Restaurants/containers/RestaurantsView')
);
const MealsView = React.lazy(() => import('../../screens/Meals/containers/MealsView'));
const MealsTypesView = React.lazy(
  () => import('../../screens/MealsTypes/containers/MealsTypesView')
);

const Routing: React.FC = () => (
  <Suspense fallback={<Loader />}>
    <Switch>
      <Route path="/meals">
        <MealsView />
      </Route>
      <Route path="/meals-types">
        <MealsTypesView />
      </Route>
      <Route path="/restaurants">
        <RestaurantView />
      </Route>
      <Route path="/">
        <Landing />
      </Route>
      <Route path="/*">
        <Redirect to="/" />
      </Route>
    </Switch>
  </Suspense>
);

export default Routing;
