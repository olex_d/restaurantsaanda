import React from 'react';
import { Router } from 'react-router-dom';
import { historyHelper } from '../../helpers/history.helper';
import Routing from '../Routing';

const App: React.FC = () => (
  <Router history={historyHelper}>
    <Routing />
  </Router>
);

export default App;
