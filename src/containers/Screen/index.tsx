import React from 'react';
import Navbar from '../../components/Navbar';
import Loader from '../../components/Loader';

interface IScreenProps {
  isLoading?: boolean;
}

const Screen: React.FC<IScreenProps> = ({ isLoading = false, children }) => (
  <>
    {isLoading ? (
      <Loader />
    ) : (
      <>
        <Navbar />
        <div>{children}</div>
      </>
    )}
  </>
);

export default Screen;
