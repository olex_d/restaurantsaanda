export interface IViewingPhoto {
  title?: string;
  photo?: string;
}
