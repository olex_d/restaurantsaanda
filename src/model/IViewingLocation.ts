export interface IViewingLocation {
  placeTitle?: string;
  address?: string;
  lat?: number;
  lng?:number;
}
