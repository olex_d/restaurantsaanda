export interface IRestaurant {
  id?: number;
  name: string;
  description: string;
  location: string;
  photoUrl?: string;
  lat?: number;
  lng?: number;
}
