import { IMealType } from '../IMealType';

export interface IMeal {
  id?: number;
  name: string;
  description: string;
  photoUrl?: string;
  price: number;
  type: IMealType;
}
