import { getOsEnv } from './helpers/path.helper';

export const env = {
  server: {
    url: getOsEnv('REACT_APP_SERVER_URL')
  },
  imgur: {
    clientId: getOsEnv('REACT_APP_IMGUR_CLIENT_ID')
  },
  maps: {
    googleMapKey: getOsEnv('REACT_APP_GOOGLE_MAPS_API_KEY')
  }
};
