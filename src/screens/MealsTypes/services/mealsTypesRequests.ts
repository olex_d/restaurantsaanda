import API from '../../../helpers/api.helper';
import { IMealType } from '../../../model/IMealType';

export const getMealsTypes = async (): Promise<IMealType[]> =>
  (await API.get('/types')).data as IMealType[];

export const getMealsTypesByName = async (name: string): Promise<IMealType[]> =>
  (await API.get('/types/like-name', { params: { name } })).data as IMealType[];

export const createMealType = async (newMealType: IMealType): Promise<void> =>
  await API.post('/types', { newMealType });

export const editMealType = async (editMealType: IMealType): Promise<void> =>
  await API.put('/types', { editMealType });

export const deleteMealType = async (deleteMealTypeId: number): Promise<void> =>
  await API.delete('/types', { params: { deleteMealTypeId } });
