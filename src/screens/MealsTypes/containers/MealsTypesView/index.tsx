import React, { useCallback, useEffect, useState } from 'react';
import Screen from '../../../../containers/Screen';
import TableHeader from '../../../../components/TableHeader';
import {Button, Form, Modal, Table} from 'react-bootstrap';
import styles from '../../../Restaurants/containers/RestaurantsView/styles.module.scss';
import ActionsRow from '../../../../components/ActionsRow';
import DeleteModal from '../../../../components/DeleteModal';
import { IMealType } from '../../../../model/IMealType';
import ModifyModal from 'screens/MealsTypes/components/ModifyModal';
import {
    createMealType,
    deleteMealType,
    editMealType,
    getMealsTypes, getMealsTypesByName
} from '../../services/mealsTypesRequests';
import Loader from '../../../../components/Loader';

const MealsTypesView: React.FC = () => {
  const [isFetching, setIsFetching] = useState(false);
  const [mealsTypes, setMealsTypes] = useState<IMealType[]>([]);
  const [isModifying, setModifying] = useState(false);
  const [editingMealType, setEditingMealType] = useState<IMealType>();
  const [deleteMealTypeId, setDeleteMealTypeId] = useState<number>();
  const [filter, setFilter] = useState( "");

  const fetchMealsTypes = useCallback(async () => {
    setIsFetching(true);
    var mealsTypes:IMealType[];
    mealsTypes  = await getMealsTypesByName(filter.toLowerCase());
    setMealsTypes(mealsTypes);
    setIsFetching(false);
  }, [filter]);

  useEffect(() => {
    fetchMealsTypes().catch(err => console.error(err));
  }, [fetchMealsTypes]);

  const getMealTypeById = useCallback(
    (mealTypeId?: number): IMealType | undefined =>
      mealsTypes.find(mealType => mealType.id === mealTypeId),
    [mealsTypes]
  );

  const handleEditClick = useCallback(
    (mealTypeId?: number) => {
      const mealType = getMealTypeById(mealTypeId);
      if (mealType) {
        setEditingMealType(mealType);
        setModifying(true);
      }
    },
    [getMealTypeById]
  );

  const handleSave = useCallback(
    async (modifiedMealType: IMealType) => {
      try {
        if (modifiedMealType?.id === undefined) await createMealType(modifiedMealType);
        else await editMealType(modifiedMealType);

        await fetchMealsTypes();
      } catch (err) {
        console.error(err);
      }
    },
    [fetchMealsTypes]
  );

  const handleModifyClose = () => {
    setModifying(false);
    setEditingMealType(undefined);
  };

  const handleDeleteClick = useCallback(
    (mealTypeId?: number) => setDeleteMealTypeId(mealTypeId),
    []
  );

  const handleDelete = useCallback(async () => {
    if (deleteMealTypeId) {
      await deleteMealType(deleteMealTypeId);
      await fetchMealsTypes();
    }
  }, [deleteMealTypeId, fetchMealsTypes]);

  return (
    <Screen>
      <Form>
          <Form.Group controlId="Filter">
              <Form.Control
                  type={'text'}
                  value={filter}
                  onChange={e => {
                      setFilter(e.target.value);
                  }}
              />
          </Form.Group>
      </Form>
      <TableHeader
        title={'Meals Types details'}
        addTitle={'Add new meal type'}
        onAddAction={() => setModifying(true)}
      />
      <Table bordered hover variant="dark">
        <thead>
          <tr className={styles.tableRow}>
            <th>ID</th>
            <th>Name</th>
            <th colSpan={2}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {isFetching ? (
            <tr>
              <td>
                <Loader loaderType={'container'} />
              </td>
            </tr>
          ) : (
            <>
              {mealsTypes.map(mealType => (
                <tr key={mealType.id} className={styles.tableRow}>
                  <td>{mealType.id}</td>
                  <td>{mealType.name}</td>
                  <ActionsRow
                    onEditClick={() => handleEditClick(mealType.id)}
                    onDeleteClick={() => handleDeleteClick(mealType.id)}
                  />
                </tr>
              ))}
            </>
          )}
        </tbody>
      </Table>
      {isModifying && (
        <ModifyModal
          editMealType={editingMealType}
          onSave={handleSave}
          onClose={handleModifyClose}
        />
      )}
      {deleteMealTypeId !== undefined && (
        <DeleteModal
          deleteObjectName={'meal type'}
          onCancel={() => setDeleteMealTypeId(undefined)}
          onDelete={handleDelete}
        />
      )}
    </Screen>
  );
};

export default MealsTypesView;
