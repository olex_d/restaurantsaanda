import React, { useCallback, useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { IBindingAction, IBindingCallback1 } from '../../../../model/callback';
import { IMealType } from '../../../../model/IMealType';
import Loader from '../../../../components/Loader';

interface IModifyModalProps {
  editMealType?: IMealType;
  onSave: IBindingCallback1<IMealType>;
  onClose: IBindingAction;
}

const ModifyModal: React.FC<IModifyModalProps> = ({ editMealType, onSave, onClose }) => {
  const [id] = useState(editMealType?.id);
  const [name, setName] = useState(editMealType?.name || '');
  const [isSaving, setSaving] = useState(false);

  const handleSave = useCallback(() => {
    setSaving(true);
    onSave({ id, name });
    onClose();
  }, [id, name, onClose, onSave]);

  return (
    <Modal show={true} onHide={onClose}>
      <>
        {isSaving ? (
          <Loader loaderType={'container'} />
        ) : (
          <>
            <Modal.Header closeButton>
              <Modal.Title>
                {editMealType ? `Edit meal type "${editMealType.name}"` : 'Add meal type'}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="formGroupName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type={'text'}
                    value={name}
                    onChange={e => setName(e.target.value)}
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="outline-warning" onClick={onClose}>
                Cancel
              </Button>
              <Button variant="warning" onClick={handleSave}>
                {editMealType ? 'Save changes' : 'Add meal type'}
              </Button>
            </Modal.Footer>
          </>
        )}
      </>
    </Modal>
  );
};

export default ModifyModal;
