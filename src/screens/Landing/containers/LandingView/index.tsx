import React from 'react';
import Screen from '../../../../containers/Screen';
import styles from './styles.module.scss';
import FeaturesRow from '../../components/FeaturesRow';
import ContactsRow from '../../components/ContactsRow';
import TitleRow from '../../components/TitleRow';
import ChatInfoRow from '../../components/ChatInfoRow';

const Landing: React.FC = () => {
  return (
    <Screen>
      <div className={styles.landingContainer}>
        <TitleRow />
        <FeaturesRow />
        <ChatInfoRow />
      </div>
      <ContactsRow />
    </Screen>
  );
};

export default Landing;
