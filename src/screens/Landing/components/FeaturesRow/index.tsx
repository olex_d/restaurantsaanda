import React from 'react';
import styles from './styles.module.scss';
import { faLeanpub } from '@fortawesome/free-brands-svg-icons';
import { faReact } from '@fortawesome/free-brands-svg-icons';
import FeatureCard from '../FeatureCard';
import { faTable } from '@fortawesome/free-solid-svg-icons';

const FeaturesRow: React.FC = () => (
  <div className={styles.featuresRowContainer}>
    <FeatureCard
      icon={faLeanpub}
      message={'Easy and understandable interface with zero learning curvature'}
    />
    <FeatureCard
      icon={faTable}
      message={'Informative tables and Google Maps and Imgur integration'}
    />
    <FeatureCard
      icon={faReact}
      message={'Efficient and fast interaction with last React and Golang tech stack'}
    />
  </div>
);

export default FeaturesRow;
