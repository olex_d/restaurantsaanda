import React from 'react';
import styles from './styles.module.scss';
import ChatMessage from '../ChatMessage';
import { Link } from 'react-router-dom';

const ChatInfoRow: React.FC = () => (
  <div className={styles.chatContainer}>
    <ChatMessage
      message={
        'Have you ever was frustrated with how tedious and unconventional restaurant management could be?'
      }
      type={'bot'}
    />
    <ChatMessage message={'Of course!'} type={'client'} />
    <ChatMessage message={'Luckily, I know the best solution for this riddle :)'} type={'bot'} />
    <ChatMessage message={'Please, share this knowledge with me'} type={'client'} />
    <ChatMessage message={'Sure, just check this link'} type={'bot'} />{' '}
    <Link to={'/restaurants'} className={styles.messageLink}>
      <ChatMessage message={'Proceed to the app'} type={'link'} />
    </Link>
  </div>
);

export default ChatInfoRow;
