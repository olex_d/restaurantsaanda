import React from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faBitbucket,
  faFacebookSquare,
  faLinkedin,
  faTwitterSquare
} from '@fortawesome/free-brands-svg-icons';

const ContactsRow: React.FC = () => (
  <div className={styles.contactsContainer}>
    <div className={styles.locationBlock}>
      <h4>Location</h4>
      <p>
        Faculty of Computer Science and Cybernetics Akademika Hlushkova Ave, 4d, Kyiv, Ukraine,
        03680
      </p>
    </div>
    <div className={styles.findOnWebBlock}>
      <h4>Find us on web</h4>
      <div className={styles.sourcesRow}>
        <a
          target="_blank"
          href="https://www.facebook.com/profile.php?id=100009075237100"
          className={styles.sourceIcon}
        >
          <FontAwesomeIcon icon={faFacebookSquare} size={'3x'} />
        </a>
        <a
          target="_blank"
          href="https://twitter.com/elonmusk?lang=uk"
          className={styles.sourceIcon}
        >
          <FontAwesomeIcon icon={faTwitterSquare} size={'3x'} />
        </a>
        <a
          target="_blank"
          href="https://www.linkedin.com/in/%D0%BE%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%BD%D0%B4%D1%80-%D0%B4%D0%B0%D0%BD%D0%B8%D0%BB%D1%8C%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-a5a007170/"
          className={styles.sourceIcon}
        >
          <FontAwesomeIcon icon={faLinkedin} size={'3x'} className={styles.sourceIcon} />
        </a>
        <a
          target="_blank"
          href="https://bitbucket.org/olex_d/restaurantsaanda/src/master/"
          className={styles.sourceIcon}
        >
          <FontAwesomeIcon icon={faBitbucket} size={'3x'} className={styles.sourceIcon} />
        </a>
      </div>
    </div>
    <div className={styles.aboutUsBlock}>
      <h4>About us</h4>
      <p>
        We are the team of young and ambitious software developers, who want to deliver high-level
        crafted business solutions
      </p>
    </div>
  </div>
);

export default ContactsRow;
