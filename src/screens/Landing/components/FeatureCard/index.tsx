import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import styles from './styles.module.scss';

interface IFeatureCardProps {
  icon: IconDefinition;
  message: string;
}

const FeatureCard: React.FC<IFeatureCardProps> = ({ icon, message }) => (
  <div className={styles.featureCard}>
    <div style={{ justifySelf: 'center' }}>
      <FontAwesomeIcon icon={icon} size={'4x'} />
      <p className={styles.featureCardMessage}>{message}</p>
    </div>
  </div>
);

export default FeatureCard;
