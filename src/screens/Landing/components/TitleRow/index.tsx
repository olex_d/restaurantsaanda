import React from 'react';
import styles from './styles.module.scss';
import { Image } from 'react-bootstrap';

const TitleRow: React.FC = () => (
  <div className={styles.titleContainer}>
    <Image src={'https://svgshare.com/i/PKh.svg'} alt="Logo" fluid className={styles.logoImage} />
    <div>
      <h1>Restaurants A&A</h1>
      <p>Best management tool for restaurant business you've ever imagined</p>
    </div>
  </div>
);

export default TitleRow;
