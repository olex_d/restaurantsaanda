import React, { useCallback } from 'react';
import styles from './styles.module.scss';

type MessageType = 'bot' | 'client' | 'link';

interface IChatMessageProps {
  message: string;
  type: MessageType;
}

const ChatMessage: React.FC<IChatMessageProps> = ({ message, type }) => {
  const getMessageStyle = useCallback((): string => {
    switch (type) {
      case 'bot':
        return styles.chatMessageBot;
      case 'client':
        return styles.chatMessageClient;
      default:
        return styles.chatMessageBotLink;
    }
  }, [type]);

  return <div className={getMessageStyle()}>{message}</div>;
};
export default ChatMessage;
