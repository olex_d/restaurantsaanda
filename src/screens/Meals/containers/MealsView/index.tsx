import React, { useCallback, useEffect, useState } from 'react';
import Screen from '../../../../containers/Screen';
import { IViewingPhoto } from '../../../../model/IViewingPhoto';
import TableHeader from '../../../../components/TableHeader';
import { Table } from 'react-bootstrap';
import styles from '../../../Restaurants/containers/RestaurantsView/styles.module.scss';
import ActionsRow from '../../../../components/ActionsRow';
import PhotoModal from '../../../../components/PhotoModal';
import DeleteModal from '../../../../components/DeleteModal';
import { IMeal } from '../../../../model/meal/IMeal';
import RowPhotoWrapper from '../../../../components/RowPhotoWrapper';
import ModifyModal from '../../components/ModifyModal';
import {createMeal, deleteMeal, editMeal, getMeals} from '../../services/mealsRequests';
import { fromNumberToCurrency } from '../../services/currencyHandler';
import Loader from '../../../../components/Loader';

const MealsView: React.FC = () => {
  const [isFetching, setIsFetching] = useState(false);
  const [meals, setMeals] = useState<IMeal[]>([]);
  const [isModifying, setModifying] = useState(false);
  const [editingMeal, setEditingMeal] = useState<IMeal>();
  const [deleteMealId, setDeleteMealId] = useState<number>();
  const [viewingMealPhoto, setViewingMealPhoto] = useState<IViewingPhoto>();

  const fetchMeals = useCallback(async () => {
    setIsFetching(true);
    const meals = await getMeals();
    setMeals(meals);
    setIsFetching(false);
  }, []);

  useEffect(() => {
    fetchMeals().catch(err => console.error(err));
  }, [fetchMeals]);

  const getMealById = useCallback(
    (mealId?: number): IMeal | undefined => meals.find(meal => meal.id === mealId),
    [meals]
  );

  const handlePhotoClick = useCallback(
    (mealId?: number) => {
      const meal = getMealById(mealId);
      if (meal) setViewingMealPhoto({ title: meal.name, photo: meal?.photoUrl });
    },
    [getMealById]
  );

  const handleEditClick = useCallback(
    (mealId?: number) => {
      const meal = getMealById(mealId);
      if (meal) {
        setEditingMeal(meal);
        setModifying(true);
      }
    },
    [getMealById]
  );

  const handleSave = useCallback(
    async (modifiedMeal: IMeal) => {
      try {
        if (modifiedMeal?.id === undefined) await createMeal(modifiedMeal);
        else await editMeal(modifiedMeal);

        await fetchMeals();
      } catch (err) {
        console.error(err);
      }
    },
    [fetchMeals]
  );

  const handleModifyClose = () => {
    setModifying(false);
    setEditingMeal(undefined);
  };

  const handleDeleteClick = useCallback((mealId?: number) => setDeleteMealId(mealId), []);

  const handleDelete = useCallback(async () => {
    if (deleteMealId) {
      await deleteMeal(deleteMealId);
      await fetchMeals();
    }
  }, [deleteMealId, fetchMeals]);

  return (
    <Screen>
      <TableHeader
        title={'Meals details'}
        addTitle={'Add new meal'}
        onAddAction={() => setModifying(true)}
      />
      <Table bordered hover variant="dark">
        <thead>
          <tr className={styles.tableRow}>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Photo</th>
            <th>Price</th>
            <th>Type</th>
            <th colSpan={2}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {isFetching ? (
            <tr>
              <td>
                <Loader loaderType={'container'} />
              </td>
            </tr>
          ) : (
            <>
              {meals.map(meal => (
                <tr key={meal.id} className={styles.tableRow}>
                  <td>{meal.id}</td>
                  <td>{meal.name}</td>
                  <td>{meal.description}</td>
                  <td>
                    <RowPhotoWrapper
                      photoUrl={meal.photoUrl}
                      altText={'Meal photo'}
                      onOpen={() => handlePhotoClick(meal?.id)}
                    />
                  </td>
                  <td>{fromNumberToCurrency(meal.price)}</td>
                  <td>{meal.type.name}</td>
                  <ActionsRow
                    onEditClick={() => handleEditClick(meal.id)}
                    onDeleteClick={() => handleDeleteClick(meal.id)}
                  />
                </tr>
              ))}
            </>
          )}
        </tbody>
      </Table>
      {viewingMealPhoto && (
        <PhotoModal photoObject={viewingMealPhoto} onClose={() => setViewingMealPhoto(undefined)} />
      )}
      {isModifying && (
        <ModifyModal editMeal={editingMeal} onSave={handleSave} onClose={handleModifyClose} />
      )}
      {deleteMealId !== undefined && (
        <DeleteModal
          deleteObjectName={'meal'}
          onCancel={() => setDeleteMealId(undefined)}
          onDelete={handleDelete}
        />
      )}
    </Screen>
  );
};

export default MealsView;
