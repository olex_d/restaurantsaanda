import React from 'react';
import { Form } from 'react-bootstrap';
import AsyncSelect from 'react-select/async';
import { useCallback, useEffect, useState } from 'react';
import { IMealType } from '../../../../model/IMealType';
import { ISelectOption } from '../../../../model/ISelectOption';
import {
  getMealsTypes,
  getMealsTypesByName
} from '../../../MealsTypes/services/mealsTypesRequests';
import debounce from 'lodash.debounce';
import { IMeal } from '../../../../model/meal/IMeal';
import { IBindingCallback1 } from '../../../../model/callback';
import { ValueType } from 'react-select';
import { styles } from './styles';

interface MealsTypesSelector {
  editMeal?: IMeal;
  onChange: IBindingCallback1<IMealType>;
}

const MIN_EXECUTING_INPUT_LENGTH = 3;
const DEFAULT_LOADING_MESSAGE = 'Loading...';
const DEFAULT_NO_OPTIONS_MESSAGE = 'Specified type cannot be found';

const MealsTypesSelector: React.FC<MealsTypesSelector> = ({ editMeal, onChange }) => {
  const [mealsTypes, setMealsTypes] = useState<IMealType[]>([]);
  const [mealsTypesOptions, setMealsTypesOptions] = useState<ISelectOption[]>([]);
  const [selectedTypeOption, setSelectedTypeOption] = useState<ISelectOption>();

  const [isOptionsLoading, setOptionsLoading] = useState<boolean>();
  const [loadingMessage, setLoadingMessage] = useState(DEFAULT_LOADING_MESSAGE);
  const [inputValue, setInputValue] = useState('');

  const fetchMealsTypes = useCallback(async () => {
    const mealsTypes = await getMealsTypes();
    setMealsTypes(mealsTypes);

    if (editMeal?.type?.id) {
      const selectedType = mealsTypes.find(type => type.id === editMeal?.type?.id);
      if (selectedType && selectedType?.id) {
        setSelectedTypeOption({ value: selectedType.id, label: selectedType.name });
        onChange(selectedType);
      }
    }
  }, [editMeal, onChange]);

  useEffect(() => {
    fetchMealsTypes().catch(err => console.error(err));
  }, [fetchMealsTypes]);

  const mapMealsTypesToOptions = useCallback(
    (mealsTypes: IMealType[]): ISelectOption[] =>
      mealsTypes
        .filter(type => type?.id !== undefined)
        .map(type => ({ value: type.id || 0, label: type.name })),
    []
  );

  useEffect(() => {
    if (mealsTypes) {
      setMealsTypesOptions(mapMealsTypesToOptions(mealsTypes));
    }
  }, [mapMealsTypesToOptions, mealsTypes]);

  const loadMealsTypesOptions = async (inputText: string) => {
    if (inputText.length <= MIN_EXECUTING_INPUT_LENGTH) {
      return [];
    }

    const normalizedInput = inputText.toLowerCase();
    const filteredMealsTypes = await getMealsTypesByName(normalizedInput);

    if (!filteredMealsTypes.length) {
      setLoadingMessage(DEFAULT_NO_OPTIONS_MESSAGE);
      setOptionsLoading(false);
    }

    return mapMealsTypesToOptions(filteredMealsTypes);
  };

  const debouncedLoadMealsTypesOptions = useCallback(
    debounce((inputValue, callback) => {
      loadMealsTypesOptions(inputValue).then(options => callback(options));
    }, 300),
    []
  );

  const handleSelect = useCallback(
    (selectedType: ValueType<ISelectOption>) => {
      if (selectedType) {
        const selectedTypeOption = selectedType as ISelectOption;
        setSelectedTypeOption(selectedTypeOption);

        const entitySelectedType = mealsTypes.find(type => type.id === selectedTypeOption.value);
        if (entitySelectedType) {
          onChange(entitySelectedType);
        }
      }
    },
    [mealsTypes, onChange]
  );

  const handleInput = useCallback((inputValue: string) => {
    if (!inputValue.length) {
      setOptionsLoading(false);
    } else if (inputValue.length <= MIN_EXECUTING_INPUT_LENGTH) {
      setOptionsLoading(true);
      setLoadingMessage('Enter 3 or more characters for searching');
    } else {
      setLoadingMessage(DEFAULT_LOADING_MESSAGE);
    }
    setInputValue(inputValue);
  }, []);

  return (
    <Form.Group controlId="formGroupType">
      <Form.Label>Type</Form.Label>
      <AsyncSelect
        isLoading={isOptionsLoading || false}
        placeholder={''}
        cacheOptions
        defaultOptions={mealsTypesOptions}
        loadOptions={debouncedLoadMealsTypesOptions}
        value={selectedTypeOption}
        onChange={handleSelect}
        onInputChange={handleInput}
        inputValue={inputValue}
        styles={styles}
        loadingMessage={() => loadingMessage}
      />
    </Form.Group>
  );
};

export default MealsTypesSelector;
