import React, { useCallback, useState } from 'react';
import { Button, Form, InputGroup, Modal } from 'react-bootstrap';
import { IBindingAction, IBindingCallback1 } from '../../../../model/callback';
import { IMeal } from '../../../../model/meal/IMeal';
import { IMealType } from '../../../../model/IMealType';
import PhotoUploader from '../../../../components/PhotoUploader';
import { uploadImgurImage } from '../../../../helpers/imgur.helper';
import MealsTypesSelector from '../MealsTypesSelector';
import Loader from '../../../../components/Loader';

interface IModifyModalProps {
  editMeal?: IMeal;
  onSave: IBindingCallback1<IMeal>;
  onClose: IBindingAction;
}

const ModifyModal: React.FC<IModifyModalProps> = ({ editMeal, onSave, onClose }) => {
  const [id] = useState(editMeal?.id);
  const [name, setName] = useState(editMeal?.name || '');
  const [description, setDescription] = useState(editMeal?.description || '');
  const [price, setPrice] = useState(editMeal?.price || 0);
  const [type, setType] = useState<IMealType>();
  const [previewPhoto, setPreviewPhoto] = useState<File>();
  const [isSaving, setSaving] = useState(false);

  const handleSave = useCallback(async () => {
    if (!type) return;

    const photoUrl =
      !previewPhoto && editMeal && editMeal?.photoUrl
        ? editMeal.photoUrl
        : await uploadImgurImage(previewPhoto);
    if (photoUrl) {
      setSaving(true);
      onSave({
        id,
        name,
        description,
        photoUrl,
        price,
        type
      });
      onClose();
    }
  }, [description, editMeal, id, name, onClose, onSave, previewPhoto, price, type]);

  const handlePhotoUpload = useCallback(async (image: File) => {
    setPreviewPhoto(image);
  }, []);

  return (
    <Modal show={true} onHide={onClose}>
      <>
        {isSaving ? (
          <Loader loaderType={'container'} />
        ) : (
          <>
            {' '}
            <Modal.Header closeButton>
              <Modal.Title>{editMeal ? `Edit meal "${editMeal.name}"` : 'Add meal'}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="formGroupName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type={'text'}
                    value={name}
                    onChange={e => setName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="formGroupDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                  />
                </Form.Group>
                <PhotoUploader
                  onPhotoUpload={handlePhotoUpload}
                  initialPhotoUrl={editMeal?.photoUrl}
                />
                <Form.Group controlId="formGroupPrice">
                  <Form.Label>Price</Form.Label>
                  <InputGroup>
                    <Form.Control
                      type={'number'}
                      value={price.toString()}
                      onChange={e => setPrice(+e.target.value)}
                      min={1}
                    />
                    <InputGroup.Append>
                      <InputGroup.Text id="inputGroupPrepend">$</InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                </Form.Group>
                <MealsTypesSelector editMeal={editMeal} onChange={setType} />
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="outline-warning" onClick={onClose}>
                Cancel
              </Button>
              <Button variant="warning" onClick={handleSave}>
                {editMeal ? 'Save changes' : 'Add meal'}
              </Button>
            </Modal.Footer>
          </>
        )}
      </>
    </Modal>
  );
};

export default ModifyModal;
