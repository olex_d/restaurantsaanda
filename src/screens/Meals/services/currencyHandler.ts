const CURRENCY_SIGN = '$';

export const fromCurrencyToNumber = (currency: string): number =>
  +currency.slice().replace(/\D/g, '');
export const fromNumberToCurrency = (number: number): string => String(number) + CURRENCY_SIGN;
