import API from '../../../helpers/api.helper';
import { IMeal } from '../../../model/meal/IMeal';

export const getMeals = async (): Promise<IMeal[]> => (await API.get('/meals')).data as IMeal[];

export const createMeal = async (newMeal: IMeal): Promise<void> =>
  await API.post('/meals', { newMeal });

export const editMeal = async (editMeal: IMeal): Promise<void> =>
  await API.put('/meals', { editMeal });

export const deleteMeal = async (deleteMealId: number): Promise<void> =>
  await API.delete('/meals', { params: { deleteMealId } });
