import API from '../../../helpers/api.helper';
import { IRestaurant } from '../../../model/restaurant/IRestaurant';

export const getRestaurants = async (): Promise<IRestaurant[]> =>
  (await API.get('/restaurants')).data as IRestaurant[];

export const createRestaurant = async (newRestaurant: IRestaurant): Promise<void> =>
  await API.post('/restaurants', { newRestaurant });

export const editRestaurant = async (editRestaurant: IRestaurant): Promise<void> =>
  await API.put('/restaurants', { editRestaurant });

export const deleteRestaurant = async (deleteRestaurantId: number): Promise<void> =>
  await API.delete('/restaurants', { params: { deleteRestaurantId } });
