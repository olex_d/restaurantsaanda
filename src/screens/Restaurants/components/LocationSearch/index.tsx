import React, { useCallback, useEffect, useState } from 'react';
import usePlacesAutocomplete from 'use-places-autocomplete';
import useOnclickOutside from 'react-cool-onclickoutside';
import { IViewingLocation } from '../../../../model/IViewingLocation';
import {
  Combobox,
  ComboboxInput,
  ComboboxList,
  ComboboxOption,
  ComboboxPopover
} from '@reach/combobox';
import '@reach/combobox/styles.css';
import { IBindingCallback1 } from '../../../../model/callback';
import MapView from '../../../../components/MapView';
import Loader from '../../../../components/Loader';
import styles from './styles.module.scss';

interface ILocationSearchProps {
  editRestaurant?: IViewingLocation;
  onChange: IBindingCallback1<string>;
}

const LocationSearch: React.FC<ILocationSearchProps> = ({ editRestaurant, onChange }) => {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions
  } = usePlacesAutocomplete({
    requestOptions: {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      location: {
        lat: () => 2,
        lng: () => 2
      },
      radius: 200 * 1000
    },
    debounce: 300
  });
  const comboBoxRef = useOnclickOutside(() => {
    clearSuggestions();
  });

  const [inputLocation, setInputLocation] = useState(editRestaurant);

  const handleListClick = useCallback(
    address => {
      setValue(address, false);
      clearSuggestions();
      setInputLocation({ address });
    },
    [clearSuggestions, setValue]
  );

  const handleMapClick = useCallback((address: string) => setValue(address, false), [setValue]);

  useEffect(() => {
    onChange(value);
  }, [onChange, value]);

  useEffect(() => {
    if (inputLocation?.address) setValue(inputLocation?.address, false);
  }, [inputLocation, setValue]);

  return (
    <>
      {!ready ? (
        <Loader loaderType={'container'} />
      ) : (
        <>
          <div className={styles.searchLocationContainer}>
            <Combobox ref={comboBoxRef} onSelect={handleListClick}>
              <ComboboxInput
                value={value}
                onChange={(e: any) => setValue(e.target.value)}
                disabled={!ready}
              />
              <div className={styles.searchLocationPopover}>
                <ComboboxPopover portal={false}>
                  <ComboboxList>
                    {status === 'OK' &&
                      data.map(({ id, description }) => (
                        <ComboboxOption key={id} value={description} />
                      ))}
                  </ComboboxList>
                </ComboboxPopover>
              </div>
            </Combobox>
          </div>
          <MapView
            interactionType={'edit'}
            locations={[inputLocation || {}]}
            onClickAddress={handleMapClick}
          />
        </>
      )}
    </>
  );
};

export default LocationSearch;
