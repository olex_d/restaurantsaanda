import React, { useCallback } from 'react';
import { Modal } from 'react-bootstrap';
import { IBindingAction } from '../../../../model/callback';
import { IViewingLocation } from '../../../../model/IViewingLocation';
import MapView from '../../../../components/MapView';

interface ILocationModalProps {
  restaurant: IViewingLocation;
  onClose: IBindingAction;
}

const LocationViewModal: React.FC<ILocationModalProps> = ({ restaurant, onClose }) => {
  const isAddressValidCallback = useCallback(
    (isValid: boolean) => {
      if (!isValid) onClose();
    },
    [onClose]
  );

  return (
    <Modal show={!!restaurant?.address} onHide={onClose}>
      <Modal.Body>
        <MapView
          interactionType={'view'}
          locations={[restaurant]}
          validationCallback={isAddressValidCallback}
        />
      </Modal.Body>
    </Modal>
  );
};

export default LocationViewModal;
