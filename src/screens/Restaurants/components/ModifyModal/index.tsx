import React, { useCallback, useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { IBindingAction, IBindingCallback1 } from '../../../../model/callback';
import { IRestaurant } from '../../../../model/restaurant/IRestaurant';
import { uploadImgurImage } from '../../../../helpers/imgur.helper';
import PhotoUploader from '../../../../components/PhotoUploader';
import LocationSearch from '../LocationSearch';
import Loader from '../../../../components/Loader';
import Geocode from "react-geocode";

interface IModifyModalProps {
  editRestaurant?: IRestaurant;
  onSave: IBindingCallback1<IRestaurant>;
  onClose: IBindingAction;
}

const ModifyModal: React.FC<IModifyModalProps> = ({ editRestaurant, onSave, onClose }) => {
  const [id] = useState(editRestaurant?.id);
  const [name, setName] = useState(editRestaurant?.name || '');
  const [location, setLocation] = useState(editRestaurant?.location || '');
  const [description, setDescription] = useState(editRestaurant?.description || '');
  const [previewPhoto, setPreviewPhoto] = useState<File>();
  const [isSaving, setSaving] = useState(false);

  const handleSave = useCallback(async () => {
    setSaving(true);
    const photoUrl =
      !previewPhoto && editRestaurant?.photoUrl
        ? editRestaurant.photoUrl
        : await uploadImgurImage(previewPhoto);

    var lat:number = 1;
    var lng:number = 1;

    await Geocode.fromAddress(location).then(
          response => {
              const latLng = response.results[0].geometry.location;
              lat = latLng.lat;
              lng = latLng.lng;
          },
          error => {
              console.error(error);
          });

    if (photoUrl) {
      onSave({ id, name, description, location, photoUrl , lat, lng});
      onClose();
    }
  }, [description, editRestaurant, id, location, name, onClose, onSave, previewPhoto]);

  const handlePhotoUpload = useCallback(async (image: File) => {
    setPreviewPhoto(image);
  }, []);

  return (
    <Modal show={true} onHide={onClose}>
      <>
        {isSaving ? (
          <Loader loaderType={'container'} />
        ) : (
          <>
            <Modal.Header closeButton>
              <Modal.Title>
                {editRestaurant ? `Edit restaurant "${editRestaurant.name}"` : 'Add restaurant'}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="formGroupName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type={'text'}
                    value={name}
                    onChange={e => setName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="formGroupLocation">
                  <Form.Label>Location</Form.Label>
                  <LocationSearch
                    editRestaurant={{
                      placeTitle: editRestaurant?.name,
                      address: editRestaurant?.location
                    }}
                    onChange={setLocation}
                  />
                </Form.Group>
                <Form.Group controlId="formGroupDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                  />
                </Form.Group>
                <PhotoUploader
                  onPhotoUpload={handlePhotoUpload}
                  initialPhotoUrl={editRestaurant?.photoUrl}
                />
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="outline-warning" onClick={onClose}>
                Cancel
              </Button>
              <Button variant="warning" onClick={handleSave}>
                {editRestaurant ? 'Save changes' : 'Add restaurant'}
              </Button>
            </Modal.Footer>
          </>
        )}
      </>
    </Modal>
  );
};

export default ModifyModal;
