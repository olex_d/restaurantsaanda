import React, { useCallback, useEffect, useRef, useState } from 'react';
import Screen from '../../../../containers/Screen';
import {Modal, Table} from 'react-bootstrap';
import TableHeader from '../../../../components/TableHeader';
import { IRestaurant } from '../../../../model/restaurant/IRestaurant';
import styles from './styles.module.scss';
import PhotoModal from '../../../../components/PhotoModal';
import { IViewingPhoto } from '../../../../model/IViewingPhoto';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { IViewingLocation } from '../../../../model/IViewingLocation';
import LocationViewModal from '../../components/LocationViewModal';
import ActionsRow from '../../../../components/ActionsRow';
import ModifyModal from '../../components/ModifyModal';
import DeleteModal from '../../../../components/DeleteModal';
import RowPhotoWrapper from '../../../../components/RowPhotoWrapper';
import {
  createRestaurant,
  deleteRestaurant,
  editRestaurant,
  getRestaurants
} from '../../services/restaurantsRequests';
import Loader from '../../../../components/Loader';
import { useLoadScript } from '@react-google-maps/api';
import { env } from '../../../../env';
import  MapView from "../../../../components/MapView";

const RestaurantView: React.FC = () => {
  const [isFetching, setIsFetching] = useState(false);
  const [restaurants, setRestaurants] = useState<IRestaurant[]>([]);
  const [isModifying, setModifying] = useState(false);
  const [editingRestaurant, setEditingRestaurant] = useState<IRestaurant>();
  const [deleteRestaurantId, setDeleteRestaurantId] = useState<number>();
  const [viewingRestaurantPhoto, setViewingRestaurantPhoto] = useState<IViewingPhoto>();
  const [viewingRestaurantLocation, setViewingRestaurantLocation] = useState<IViewingLocation>();

  const fetchRestaurants = useCallback(async () => {
    setIsFetching(true);
    const restaurants = await getRestaurants();
    setRestaurants(restaurants);
    setIsFetching(false);
  }, []);

  useEffect(() => {
    fetchRestaurants().catch(err => console.error(err));
  }, [fetchRestaurants]);

  const getRestaurantById = useCallback(
    (restaurantId?: number): IRestaurant | undefined =>
      restaurants.find(restaurant => restaurant.id === restaurantId),
    [restaurants]
  );

  const handlePhotoClick = useCallback(
    (restaurantId?: number) => {
      const restaurant = getRestaurantById(restaurantId);
      if (restaurant)
        setViewingRestaurantPhoto({ title: restaurant.name, photo: restaurant?.photoUrl });
    },
    [getRestaurantById]
  );

  const handleLocationClick = useCallback(
    (restaurantId?: number) => {
      const restaurant = getRestaurantById(restaurantId);
      if (restaurant)
        setViewingRestaurantLocation({
          placeTitle: restaurant.name,
          address: restaurant.location,
          lat: restaurant.lat,
          lng: restaurant.lng
        });
    },
    [getRestaurantById]
  );

  const handleEditClick = useCallback(
    (restaurantId?: number) => {
      const restaurant = getRestaurantById(restaurantId);
      if (restaurant) {
        setEditingRestaurant(restaurant);
        setModifying(true);
      }
    },
    [getRestaurantById]
  );

  const handleDeleteClick = useCallback(
    (restaurantId?: number) => setDeleteRestaurantId(restaurantId),
    []
  );

    const handleSave = useCallback(
    async (modifiedRestaurant: IRestaurant) => {
      try {
        if (modifiedRestaurant?.id === undefined) await createRestaurant(modifiedRestaurant);
        else await editRestaurant(modifiedRestaurant);

        await fetchRestaurants();
      } catch (err) {
        console.error(err);
      }
    },
    [fetchRestaurants]
  );

  const handleModifyClose = () => {
    setModifying(false);
    setEditingRestaurant(undefined);
  };

  const handleDelete = useCallback(async () => {
    if (deleteRestaurantId) {
      await deleteRestaurant(deleteRestaurantId);
      await fetchRestaurants();
    }
  }, [deleteRestaurantId, fetchRestaurants]);

  const libraries = useRef(['places']);
  const { loadError } = useLoadScript({
    googleMapsApiKey: env.maps.googleMapKey,
    libraries: libraries.current
  });

  useEffect(() => {
    if (loadError) console.error(loadError);
  }, [loadError]);

  return (
    <Screen>
      <TableHeader
        title={'Restaurants details'}
        addTitle={'Add new restaurant'}
        onAddAction={() => setModifying(true)}
      />
      <Table bordered hover variant="dark">
        <thead>
          <tr className={styles.tableRow}>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Location</th>
            <th>Photo</th>
            <th colSpan={2}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {isFetching ? (
            <tr>
              <td>
                <Loader loaderType={'container'} />
              </td>
            </tr>
          ) : (
            <>
              {restaurants.map(restaurant => (
                <tr key={restaurant.id} className={styles.tableRow}>
                  <td>{restaurant.id}</td>
                  <td>{restaurant.name}</td>
                  <td>{restaurant.description}</td>
                  <td
                    className={styles.selectable}
                    onClick={() => handleLocationClick(restaurant.id)}
                  >
                    <FontAwesomeIcon icon={faMapMarkerAlt} />
                    &nbsp;&nbsp;
                    {restaurant.location}
                  </td>
                  <td>
                    <RowPhotoWrapper
                      photoUrl={restaurant.photoUrl}
                      altText={'Restaurant photo'}
                      onOpen={() => handlePhotoClick(restaurant?.id)}
                    />
                  </td>
                  <ActionsRow
                    onEditClick={() => handleEditClick(restaurant.id)}
                    onDeleteClick={() => handleDeleteClick(restaurant.id)}
                  />
                </tr>
              ))}
            </>
          )}
        </tbody>
      </Table>
      {!isFetching && restaurants && restaurants.length > 0 &&
        <div className={styles.allLocationsView}>
            <MapView
                interactionType={'view'}
                locations={restaurants.map(restaurant => {
                    var a:IViewingLocation = {
                        placeTitle: restaurant.name,
                        address: restaurant.location,
                        lat: restaurant.lat,
                          lng: restaurant.lng
                      }
                      return a;
                  })}
              />
        </div>
      }
      {viewingRestaurantPhoto && (
        <PhotoModal
          photoObject={viewingRestaurantPhoto}
          onClose={() => setViewingRestaurantPhoto(undefined)}
        />
      )}
      {viewingRestaurantLocation && (
        <LocationViewModal
          restaurant={viewingRestaurantLocation}
          onClose={() => setViewingRestaurantLocation(undefined)}
        />
      )}
      {isModifying && (
        <ModifyModal
          editRestaurant={editingRestaurant}
          onSave={handleSave}
          onClose={handleModifyClose}
        />
      )}
      {deleteRestaurantId !== undefined && (
        <DeleteModal
          deleteObjectName={'restaurant'}
          onCancel={() => setDeleteRestaurantId(undefined)}
          onDelete={handleDelete}
        />
      )}
    </Screen>
  );
};

export default RestaurantView;
