import axios from 'axios';
import { env } from '../env';

export const getPreviewUrl = (image: File): string => URL.createObjectURL(image);

export const uploadImgurImage = async (image?: File): Promise<string | undefined> => {
  if (!image) return undefined;

  const formData = new FormData();
  formData.append('image', image);
  const response = (
    await axios.post('https://api.imgur.com/3/image', formData, {
      headers: {
        Authorization: `Client-ID ${env.imgur.clientId}`
      }
    })
  ).data;
  return response.data.link;
};
