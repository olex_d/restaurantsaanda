import React from 'react';
import { IBindingAction } from '../../model/callback';
import { Button } from 'react-bootstrap';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

interface ITableHeaderProps {
  title: string;
  addTitle: string;
  onAddAction?: IBindingAction;
}

const TableHeader: React.FC<ITableHeaderProps> = ({ title, addTitle, onAddAction }) => (
  <div className={styles.tableHeaderWrapper}>
    <h2>{title}</h2>
    <div className={styles.addButton}>
      <Button variant={'warning'} onClick={onAddAction} disabled={!onAddAction}>
        <FontAwesomeIcon icon={faPlusCircle} />
        &nbsp;&nbsp;{addTitle}
      </Button>
    </div>
  </div>
);

export default TableHeader;
