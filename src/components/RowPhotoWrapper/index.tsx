import React from 'react';
import { IBindingAction } from '../../model/callback';
import styles from '../../screens/Restaurants/containers/RestaurantsView/styles.module.scss';
import { Image } from 'react-bootstrap';

interface IRowPhotoWrapperProps {
  photoUrl?: string;
  altText?: string;
  onOpen: IBindingAction;
}

const RowPhotoWrapper: React.FC<IRowPhotoWrapperProps> = ({ photoUrl, altText = '', onOpen }) => (
  <>
    {photoUrl ? (
      <span onClick={onOpen} className={styles.selectable}>
        <Image width={60} src={photoUrl} alt={altText} rounded />
      </span>
    ) : (
      <span>Photo wasn't provided</span>
    )}
  </>
);

export default RowPhotoWrapper;
