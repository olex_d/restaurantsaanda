import React, { useCallback, useEffect, useRef, useState } from 'react';
import styles from './styles.module.scss';
import { GoogleMap, InfoWindow, Marker } from '@react-google-maps/api';
import { IViewingLocation } from '../../model/IViewingLocation';
import { LatLon } from 'use-places-autocomplete';
import { IBindingCallback1, IBindingCallback3 } from '../../model/callback';
import Geocode from 'react-geocode';
import { env } from '../../env';

type MapInteractionType = 'view' | 'edit';

interface IMapViewProps {
  interactionType: MapInteractionType;
  locations: IViewingLocation[];
  validationCallback?: IBindingCallback1<boolean>;
  onClickAddress?: IBindingCallback1<string>;
}

const DEFAULT_LAT_LNG = { lat: 0, lng: 0 };

const MapView: React.FC<IMapViewProps> = ({
  interactionType = 'view',
  locations,
  validationCallback,
  onClickAddress
}) => {
  useEffect(() => {
    Geocode.setApiKey(env.maps.googleMapKey);
  }, []);

  const mapOptions = useRef({
    disableDefaultUI: true,
    fullscreenControl: true,
    zoomControl: true,
    zoom: 3,
    markerSize: 24
  });

  const [isMapRefSetted, setMapRefSetted] = useState(false);
  const mapRef = useRef();
  const onMapLoad = useCallback(map => {
    mapRef.current = map;
    setMapRefSetted(true);
  }, []);

  const [viewingLocations, setViewingLocations] = useState<IViewingLocation[]>(locations);
  const centerRef = useRef<LatLon>(DEFAULT_LAT_LNG);
  const [isSelected, setSelected] = useState(interactionType !== 'edit');

  const panTo = useCallback((location: LatLon) => {
    if (mapRef.current) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      mapRef.current.panTo(location);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      mapRef.current.setZoom(17);
    }
  }, []);

  const handleMapClick = useCallback(
    async event => {
      if (interactionType !== 'edit') return;

      const lat = event.latLng.lat();
      const lng = event.latLng.lng();
      Geocode.fromLatLng(String(event.latLng.lat()), String(event.latLng.lng())).then(
        response => {
          const address = response.results[0].formatted_address;
          if (onClickAddress) onClickAddress(address);
          setViewingLocations([{address: address,lat:lat, lng:lng}]);
        },
        error => {
          console.error(error);
          if (onClickAddress) onClickAddress('');
        }
      );
    },
    [interactionType, onClickAddress]
  );

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      position => {
        panTo({ lat: position.coords.latitude, lng: position.coords.longitude });
      },
      error => {
        console.error(error);
      }
    );
  }, [panTo]);

  return (
    <GoogleMap
      mapContainerClassName={styles.googleMapsContainer}
      center={centerRef.current}
      options={mapOptions.current}
      onLoad={onMapLoad}
      onClick={handleMapClick}
    >
      {viewingLocations && viewingLocations.map(viewingLocation => {
            if (viewingLocation?.lat && viewingLocation?.lng) {
                return <>
                    <Marker
                        position={new window.google.maps.LatLng(viewingLocation.lat, viewingLocation.lng)}
                        icon={{
                            url: 'https://svgshare.com/i/PpH.svg',
                            scaledSize: new window.google.maps.Size(
                                mapOptions.current.markerSize,
                                mapOptions.current.markerSize
                            ),
                            origin: new window.google.maps.Point(0, 0),
                            anchor: new window.google.maps.Point(
                                mapOptions.current.markerSize / 2,
                                mapOptions.current.markerSize / 2
                            )
                        }}
                        onClick={() => interactionType !== 'edit' && setSelected(!isSelected)}
                    />
                    {isSelected && (
                    <InfoWindow
                        position={new window.google.maps.LatLng(viewingLocation.lat, viewingLocation.lng)}
                        onCloseClick={() => setSelected(false)}
                        options={{
                            pixelOffset: new window.google.maps.Size(0, -mapOptions.current.markerSize / 2)
                        }}
                    >
                        <div>
                            <h5 className={styles.mapTitle}>{viewingLocation.placeTitle}</h5>
                            <h6 className={styles.mapLocation}>{viewingLocation.address}</h6>
                        </div>
                    </InfoWindow>
                    )}
                </>
            }
          }
      )}
    </GoogleMap>
  );
};

export default MapView;
