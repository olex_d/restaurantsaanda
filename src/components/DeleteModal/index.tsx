import React, { useCallback } from 'react';
import { IBindingAction } from '../../model/callback';
import { Button, Modal, Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import styles from './styles.module.scss';

interface IDeleteModalProps {
  deleteObjectName?: string;
  onCancel: IBindingAction;
  onDelete: IBindingAction;
}

const DeleteModal: React.FC<IDeleteModalProps> = ({ deleteObjectName, onCancel, onDelete }) => {
  const handleDelete = useCallback(() => {
    onDelete();
    onCancel();
  }, [onCancel, onDelete]);

  return (
    <Modal show={true} onHide={onCancel}>
      <Modal.Header closeButton>Delete {deleteObjectName}</Modal.Header>
      <Modal.Body className={styles.deleteModalBody}>
        <Container>
          <div className={styles.deleteIcon}>
            <FontAwesomeIcon icon={faTrash} size={'8x'} />
          </div>
          Do you really want to delete this{' '}
          <span className={styles.deleteObjectName}>{deleteObjectName}</span>? <br />
          This action cannot be undone!
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-warning" onClick={onCancel}>
          Cancel
        </Button>
        <Button variant="danger" onClick={handleDelete} className={styles.deleteButton}>
          Delete
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeleteModal;
