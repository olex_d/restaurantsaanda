import React from 'react';
import styles from './styles.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

type LoaderType = 'screen' | 'container';

interface ILoaderProps {
  loaderType?: LoaderType;
}

const Loader: React.FC<ILoaderProps> = ({ loaderType = 'screen' }) => (
  <div
    className={loaderType === 'screen' ? styles.loaderWrapperScreen : styles.loaderWrapperContainer}
  >
    <div className={styles.loaderIcon}>
      <FontAwesomeIcon icon={faSpinner} className={'fa-spin'} size={'5x'} />
    </div>
  </div>
);

export default Loader;
