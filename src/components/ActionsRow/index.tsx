import React from 'react';
import { IBindingAction } from '../../model/callback';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import styles from './styles.module.scss';

interface IActionsRowProps {
  onEditClick: IBindingAction;
  onDeleteClick: IBindingAction;
}

const ActionsRow: React.FC<IActionsRowProps> = ({ onEditClick, onDeleteClick }) => (
  <>
    <td className={styles.actionButtonEdit} onClick={onEditClick}>
      <FontAwesomeIcon icon={faEdit} />
    </td>
    <td className={styles.actionButtonDelete} onClick={onDeleteClick}>
      <FontAwesomeIcon icon={faTrash} />
    </td>
  </>
);

export default ActionsRow;
