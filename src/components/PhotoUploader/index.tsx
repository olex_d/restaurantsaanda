import React, { useCallback, useState } from 'react';
import { IBindingCallback1 } from '../../model/callback';
import { Form, Image } from 'react-bootstrap';
import { getPreviewUrl } from '../../helpers/imgur.helper';
import styles from './styles.module.scss';

interface IPhotoUploaderProps {
  onPhotoUpload: IBindingCallback1<File>;
  initialPhotoUrl?: string;
}

const PhotoUploader: React.FC<IPhotoUploaderProps> = ({ onPhotoUpload, initialPhotoUrl }) => {
  const [previewPhoto, setPreviewPhoto] = useState<File>();

  const getFileLabel = useCallback(() => {
    if (previewPhoto) {
      return previewPhoto.name;
    } else if (initialPhotoUrl) {
      return initialPhotoUrl;
    } else {
      return '';
    }
  }, [initialPhotoUrl, previewPhoto]);

  const handlePhotoUpload = useCallback(
    async (e: HTMLInputElement) => {
      if (!e || !e.files) return;

      const image = e.files[0];
      setPreviewPhoto(image);
      onPhotoUpload(image);
    },
    [onPhotoUpload]
  );

  return (
    <Form.Group controlId="formGroupPhoto">
      <Form.Label>Photo</Form.Label>
      <Form.File
        id="restaurant-photo"
        label={getFileLabel()}
        custom
        onChange={(e: any) => handlePhotoUpload(e.target)}
      />
      {((!previewPhoto && initialPhotoUrl) || previewPhoto) && (
        <>
          <hr />
          <div className={styles.inlineCentered}>
            <Image
              src={previewPhoto ? getPreviewUrl(previewPhoto) : initialPhotoUrl}
              fluid
              rounded
            />
          </div>
        </>
      )}
    </Form.Group>
  );
};

export default PhotoUploader;
