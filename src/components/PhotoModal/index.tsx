import React from 'react';
import { Image, Modal } from 'react-bootstrap';
import { IBindingAction } from '../../model/callback';
import { IViewingPhoto } from '../../model/IViewingPhoto';
import styles from './styles.module.scss';

interface IPhotoModalProps {
  photoObject: IViewingPhoto;
  onClose: IBindingAction;
}

const PhotoModal: React.FC<IPhotoModalProps> = ({ photoObject, onClose }) => (
  <Modal show={!!photoObject?.photo} onHide={onClose}>
    <Modal.Header closeButton>
      {photoObject.title && <Modal.Title>{photoObject.title}</Modal.Title>}
    </Modal.Header>
    <Modal.Body className={styles.inlineCentered}>
      <Image src={photoObject.photo} alt="restaurantPhoto" fluid rounded />
    </Modal.Body>
  </Modal>
);

export default PhotoModal;
