import React from 'react';
import { Nav, Navbar as BootstrapNavbar } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

const Navbar: React.FC = () => (
  <BootstrapNavbar expand="sm" bg="dark" variant="dark">
    <LinkContainer to="/">
      <BootstrapNavbar.Brand>
        <img
          alt=""
          src="https://svgshare.com/i/PKh.svg"
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{' '}
        Restaurants A&A
      </BootstrapNavbar.Brand>
    </LinkContainer>
    <BootstrapNavbar.Toggle aria-controls={'navbar-content'} />
    <BootstrapNavbar.Collapse id={'navbar-content'}>
      <Nav className="mr-auto">
        <LinkContainer to="/restaurants">
          <Nav.Link>Restaurants</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/meals">
          <Nav.Link>Meals</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/meals-types">
          <Nav.Link>Meals Types</Nav.Link>
        </LinkContainer>
      </Nav>
    </BootstrapNavbar.Collapse>
  </BootstrapNavbar>
);

export default Navbar;
